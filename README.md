**How to Run**

1. The web application is built using Django 3.0 and Python 3
2. Install the required dependencies using `pip3 install -r requirements.txt` (use `sudo` if required).
3. `python3 manage.py makemigrations`
4. `python3 manage.py migrate`
5. `python3 manage.py runserver`

**XML Data**

1. The given XML data has already been added to the database with the help of an xml parser.
2. More XML data can be uploaded by going to the homepage.
3. The form allows you to upload a valid XML file to enter data.


**API Endpoints**

1. http://127.0.0.1:8000/posts - Lists all the post data, there is a default pagination of 10 posts per page applied.
2. http://127.0.0.1:8000/posts/?orderby=score - Sorts the post data according to score data.
3. http://127.0.0.1:8000/posts/?orderby=view_count - Sorts the post data according to view counts.
4. http://127.0.0.1:8000/posts/?search=sampleword - Searches all the posts which contain 'sampleword' in the body text data.
---
