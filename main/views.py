import datetime
import xml.etree.ElementTree as ET
from django.shortcuts import render
from django.db.models import Q
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import generics
from .serializers import PostSerializer
from .models import PostModel
from .forms import submitXmlForm


def indexView(request):
    if request.method == 'POST':
        form = submitXmlForm(request.POST, request.FILES)
        if form.is_valid():
            xml_parser(request.FILES['xmlfile'])
        return render(request, 'done.html')
    form = submitXmlForm()
    return render(request, 'index.html', { 'form':form })

def xml_parser(filename):
    tree = ET.parse(filename)
    root = tree.getroot()

    for child in root:
        data = child.attrib
        postObj = PostModel()
        try:
            postObj.postId = data['Id']
        except KeyError as e:
            postObj.postId = 0

        try:
            postObj.postTypeId = data['PostTypeId']
        except KeyError as e:
            postObj.postTypeId = 0

        try:
            postObj.parentId = data['ParentId']
        except KeyError as e:
            postObj.parentId = 0
        try:
            postObj.creationDate = data['CreationDate']
        except KeyError as e:
            postObj.creationDate = datetime.datetime.now()

        try:
            postObj.score = data['Score']
        except KeyError as e:
            postObj.score = 0

        try:
            postObj.body = data['Body']
        except KeyError as e:
            postObj.body = 'no body'

        try:
            postObj.viewCount = data['ViewCount']
        except KeyError as e:
            postObj.viewCount = 0

        try:
            postObj.ownerUserId = data['OwnerUserId']
        except KeyError as e:
            postObj.ownerUserId = 0

        try:
            postObj.lastEditorUserId = data['LastEditorUserId']
        except KeyError as e:
            postObj.lastEditorUserId = 0

        try:
            postObj.lastEditDate = data['LastEditDate']
        except KeyError as e:
            postObj.lastEditDate = datetime.datetime.now()

        try:
            postObj.lastActivityDate = data['lastActivityDate']
        except KeyError as e:
            postObj.lastActivityDate = datetime.datetime.now()

        try:
            postObj.commentCount = data['CommentCount']
        except KeyError as e:
            postObj.commentCount = 0

        postObj.save()


class PostListView(generics.ListAPIView):
    serializer_class = PostSerializer

    def get_queryset(self):

        queryset = PostModel.objects.all()
        orderby = self.request.query_params.get('orderby', None)

        search = self.request.query_params.get('search', None)

        if orderby is not None:
            if orderby == 'score':
                queryset =  queryset.order_by('score')
            if orderby == 'view_count':
                queryset = queryset.order_by('viewCount')

        if search is not None:
            queryset = queryset.filter(body__contains=search)

        return queryset
