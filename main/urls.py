from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from .views import indexView, PostListView


urlpatterns = [
    path('', indexView, name='index'),
    path('posts/', PostListView.as_view(), name='postListView'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
