from django.db import models

class PostModel(models.Model):
    postId = models.IntegerField()
    postTypeId = models.IntegerField()
    parentId = models.IntegerField()
    creationDate = models.DateTimeField()
    score = models.IntegerField()
    viewCount = models.IntegerField()
    body = models.TextField()
    ownerUserId = models.IntegerField()
    lastEditorUserId = models.IntegerField()
    lastEditDate = models.DateTimeField()
    lastActivityDate = models.DateTimeField()
    commentCount = models.IntegerField()
